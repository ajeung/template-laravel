@extends('layouts.master')

@section('title')
    Show Cast {{$cast->id}}
@endsection

@section('content')
    <p>Nama : {{$cast->nama}}</p>
    <p>Umur : {{$cast->umur}}</p>
    <p>Bio  : {{$cast->bio}}</p>
@endsection